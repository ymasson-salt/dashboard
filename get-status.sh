#!/bin/bash

gitlab_url="https://gitlab.com"
branches="devel master"
reloadtime=30

groupname="ymasson-salt"
projfilter=""


col_num=0
echo "<html>
<head>
<title>${groupname} project CI status</title>
<link rel=\"stylesheet\" href=\"https://www.w3schools.com/w3css/4/w3.css\">
<style>
body {
 background-color: #101010;
 color: white;
 margin: 0;
 padding: 0;
}
h p table {
 margin: 0;
 padding: 0;
}
a {
  text-decoration: none;
}
.card {
  width: 200px;
  background-color: #151515;
  margin: 0px 5px 15px 0px;
  padding: 0px 2px 2px 5px;
  border: 1px solid grey;
  border-radius: 5px;
}
</style>
</head>
<body>
<script lang='javascript'>
setTimeout(function(){ window.location.reload(1);}, $(( ${reloadtime} * 1000)));
</script>
"

for g in ${groupname}; do
    projname=$(curl -k ${gitlab_url}/api/v4/groups/${g} 2>/dev/null | jq '.projects| .[].path_with_namespace' | awk -F '"' '{print $2}' | sort)
    echo "<div class=\"w3-row\" style=\"margin-left:5px;\">"
    echo "<h2><a href='${gitlab_url}/${g}'>${g}</a></h2>" 
    for p in ${projname}; do
      # skip current project when not match projfilter
      echo "${p}" | grep -q "${g}/${projfilter}" && continue
      echo "<div class=\"w3-col card\"><table>"
	    echo "<div style=\"text-align: center;\"><h5><a href='${gitlab_url}/${p}'>"
      echo "${p}" | awk -F '/' '{print $2}'
      echo "</a></h5></div>"
	    for b in ${branches}; do
		    echo "<tr><td>${b}: </td><td><a href='${gitlab_url}/${p}/commits/${b}'><img src='${gitlab_url}/${p}/badges/${b}/pipeline.svg'/></a></td></tr>"
	    done
	    echo "</table></div>"
    done
echo "</div>"
done
echo "</body></html>"
